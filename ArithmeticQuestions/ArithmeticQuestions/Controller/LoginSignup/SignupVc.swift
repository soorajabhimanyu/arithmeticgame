//
//  SignupVc.swift
//  ArithmeticQuestions
//
//  Created by Abhimanyu Rathore on 16/08/21.

import UIKit

class SignupVc: UIViewController {

    //MARK:- Model for login to save in local DB  or send data on server
    lazy var model:Signup =  {
        let signup = Signup()
        return signup
    }()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
