//  LoginVc.swift
//  ArithmeticQuestions
//
//  Created by Abhimanyu Rathore on 16/08/21.
//

import UIKit

class LoginVc: UIViewController {
    
    //MARK: UI Objects
    
    //MARK: Model for login to save in local DB or send data on server
    lazy var model:Login =  {
        let login = Login()
        return login
    }()
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: UIButton, IBAction functions
    
    @IBAction func LoginButton(_ sender: Any) {
    }
    
    @IBOutlet weak var PasswordTextField: UITextField!
    
    @IBOutlet weak var EmailTextField: UITextField!
    
    
}
