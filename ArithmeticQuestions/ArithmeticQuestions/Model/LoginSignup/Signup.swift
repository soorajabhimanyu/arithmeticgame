//
//  Signup.swift
//  ArithmeticQuestions
//
//  Created by Sooraj Coding on 8/21/21.
//

import Foundation

public class Signup {

    var name:String
    var age:Int
    var gender:String?
    
    var email:String
    var password:String

    public init(name:String = "Sooraj Sambasivam", age:Int = 15, gender:String = "Male", email:String = "sooraj@gmail.com", password:String = "@goodDay123") {
        
        self.name = name
        self.age = age
        self.gender = gender
        
        self.email = email
        self.password = password
    }
}
